This module provides an extra display field for taxonomy fields that allows
users with the right permissions to edit the taxonomy terms related to a
entity. This makes it easy for visitors to participate and extend your
content.

There are 2 different cases:
- Free tagging: For this field it is possible to let users create new tags
- Other: These fields just show the standard form to link/unlink existing terms

A typical use case for this module is when you want to extend/complete a large
dataset with the help of your visitors. They can help tagging your dataset in
the frontend, without needing permissions to change other content.


Usage
--------------
To allow a taxonomy field for public tagging you need the following steps:
1. Edit your field settings and check 'Enable public tagging for this field'.
2. Set permissions for users to edit your public tagging field.
3. Add the new form display field to your view modes.


Author
--------------
Marc Kwee - https://www.drupal.org/u/marckwee
